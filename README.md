# Các bước thực hiện để chạy project bằng docker

1.  Mở thư mục dùng để lưu trữ source code bằng terminal (linux) hoặc command line (windows)
2. Sử dụng lệnh git clone https://gitlab.com/cnpmm-thaythai/cnpmm-deployment.git để clone project
3. Sau dó cd cnpmm-deployment để truy cập vào thư mục này.
4. Chạy lệnh docker network create gateway-network
5. Chạy lệnh docker-compose pull && docker-compose up -d
6. Truy cập địa chỉ http://localhost:5193/swagger/index.html để đến trang swagger để làm việc với api.
