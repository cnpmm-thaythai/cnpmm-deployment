-- MySQL dump 10.13  Distrib 8.1.0, for Linux (x86_64)
--
-- Host: localhost    Database: ecommercedb
-- ------------------------------------------------------
-- Server version	8.1.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__EFMigrationsHistory`
--

DROP TABLE IF EXISTS `__EFMigrationsHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ProductVersion` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__EFMigrationsHistory`
--

LOCK TABLES `__EFMigrationsHistory` WRITE;
/*!40000 ALTER TABLE `__EFMigrationsHistory` DISABLE KEYS */;
INSERT INTO `__EFMigrationsHistory` VALUES ('20231118082336_UpdateProductUser','7.0.13'),('20231119154454_UpdateProductEntity','7.0.13'),('20231123030122_updateorderitem','7.0.13'),('20231128045029_UpdateEntity','7.0.13'),('20231201035205_UpdateOrderEntity','7.0.13');
/*!40000 ALTER TABLE `__EFMigrationsHistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand_entity`
--

DROP TABLE IF EXISTS `brand_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand_entity`
--

LOCK TABLES `brand_entity` WRITE;
/*!40000 ALTER TABLE `brand_entity` DISABLE KEYS */;
INSERT INTO `brand_entity` VALUES ('134ab31c-c941-4966-8959-c0ff08c0ab37','2023-11-28 11:29:55.801584',NULL,'01HGAV2R1F716N06V2W33N1K1V.webp',NULL,'Acer'),('166dc712-cf4c-412b-8089-995a888c1f44','2023-11-28 11:30:05.612058',NULL,'01HGAV31MTH42AKGM11GG27SGR.png',NULL,'Asus'),('47fc71a5-bcff-44aa-bff4-e93a9bca3bbe','2023-11-28 11:29:41.229786',NULL,'01HGAV29V5K8FQ7BQ2FV4RPANG.png',NULL,'HP'),('5bbc089b-ad2b-4525-80e4-a0ec678b90b2','2023-11-28 11:28:50.862042',NULL,'01HGAV0RMTPJ1SH11JAJTZWGMD.png',NULL,'Dell'),('5d8b3b2a-3eeb-47af-ae94-e032d01360a9','2023-12-04 02:18:14.625982',NULL,'01HGS9WWHCV50RZ411AGQERM36.png',NULL,'MSI'),('a7e95441-bd05-47fe-843e-54876395ddf8','2023-11-28 11:29:08.594418',NULL,'01HGAV19YJZHRFQYFT3W12FTHS.png',NULL,'Lenovo'),('b0d92863-cb56-4120-8f80-0e2932d7f0b5','2023-11-28 11:29:28.012967',NULL,'01HGAV1WY4F53R84DV17V0RCW3.png',NULL,'Apple');
/*!40000 ALTER TABLE `brand_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_entity`
--

DROP TABLE IF EXISTS `category_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_entity`
--

LOCK TABLES `category_entity` WRITE;
/*!40000 ALTER TABLE `category_entity` DISABLE KEYS */;
INSERT INTO `category_entity` VALUES ('892b81a5-be0d-4c4f-9850-f7058f963e93','2023-11-29 11:47:03.054892',NULL,'01HGDEET66WZNAQ0FAZCTG5HQP.png',NULL,'Màn hình','Màn hình máy tính (Computer display, Visual display unit hay Monitor) là thiết bị điện tử dùng để kết nối với máy tính nhằm mục đích hiển thị và phục vụ cho quá trình giao tiếp giữa người sử dụng với máy tính.'),('c4cb9cdb-59b5-484c-bdd3-92d86772b2ec','2023-11-29 11:39:34.636647',NULL,'01HGDE147VP27SPCVB5300CEM4.png',NULL,'Laptop','Laptop là một thiết bị máy tính có kích thước nhỏ gọn và di động. Nó được thiết kế để sử dụng trong các hoạt động làm việc, giải trí hoặc học tập khi di chuyển mà không cần phải sử dụng những chiếc máy tính để bàn cồng kềnh.'),('d150ac30-ed4d-4d27-a6a9-085d7517939c','2023-11-29 11:42:39.968341',NULL,'01HGDE6S8XVRKKMYT0BR0NT8H8.png',NULL,'Bàn phím','Bàn phím là một thiết bị đầu vào cho máy tính, được sử dụng để nhập dữ liệu và điều khiển các chức năng của máy tính. Bàn phím bao gồm một loạt các phím nhấn, các phím chữ, số, các ký tự đặc biệt và các phím chức năng để thực hiện các tác vụ.');
/*!40000 ALTER TABLE `category_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_entity`
--

DROP TABLE IF EXISTS `order_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `total` double DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `is_paid` bit(1) DEFAULT NULL,
  `payment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `receiver_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `receiver_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4na8ykxyemvs9auhondwoj7ir` (`user_id`),
  CONSTRAINT `FK4na8ykxyemvs9auhondwoj7ir` FOREIGN KEY (`user_id`) REFERENCES `user_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_entity`
--

LOCK TABLES `order_entity` WRITE;
/*!40000 ALTER TABLE `order_entity` DISABLE KEYS */;
INSERT INTO `order_entity` VALUES ('082fafa9-2776-4d76-828a-92543e8576c5','2023-12-03 07:32:11.421812',NULL,'2023-12-03 07:32:11.421812','Đã hủy',51980000,'53420496-bccf-4df5-adea-59e44cf8ec8e','Quảng',_binary '\0','banking','Trần Thị Phương Linh','0312345678'),('457f8cf2-f805-43b0-8d90-5f4865745ba3','2023-12-02 12:13:51.203584',NULL,'2023-12-02 12:13:51.203584','Đã tạo',149940000,'c8d8e43c-5a3b-4b5d-90fa-36a421611826','Quan 9',_binary '\0','Banking','A Duy','12412512512'),('4b9e343d-f68a-4935-9221-81b6fc717662','2023-12-03 07:27:17.490757',NULL,'2023-12-03 07:27:17.490757','Đã tạo',51980000,'53420496-bccf-4df5-adea-59e44cf8ec8e','Quy Nhơn',_binary '\0','banking','Huỳnh Tiến Dĩ','0312345678'),('acce1d72-54bb-40c1-bacb-3a3bd43fef0d','2023-12-02 12:13:08.079379',NULL,'2023-12-02 12:13:08.079379','Đã tạo',199920000,'c8d8e43c-5a3b-4b5d-90fa-36a421611826','Thu Duc',_binary '\0','COD','A Duy','12412512512'),('d2b58a49-cf5b-4e9b-ae4c-6f78c9e99028','2023-12-03 07:26:51.185849',NULL,'2023-12-03 07:26:51.185849','Đã tạo',51980000,'53420496-bccf-4df5-adea-59e44cf8ec8e','Số 1, Võ Văn Ngân',_binary '\0','banking','Nguyễn Ngọc Duy','0336699075'),('fab859f1-5bba-4145-821d-c41549687f4b','2023-12-03 07:21:41.292224',NULL,'2023-12-03 07:21:41.292224','Đã tạo',51980000,'53420496-bccf-4df5-adea-59e44cf8ec8e','Số 1, Võ Văn Ngân',_binary '\0','COD','Trần Văn Quảng','0336699075');
/*!40000 ALTER TABLE `order_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_entity`
--

DROP TABLE IF EXISTS `order_item_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKe17bu5pl07v13gm6pxjw8l87e` (`product_id`),
  KEY `FKfok8dv4hj3mw9wixok1qq15xr` (`order_id`),
  CONSTRAINT `FKe17bu5pl07v13gm6pxjw8l87e` FOREIGN KEY (`product_id`) REFERENCES `product_entity` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKfok8dv4hj3mw9wixok1qq15xr` FOREIGN KEY (`order_id`) REFERENCES `order_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_entity`
--

LOCK TABLES `order_item_entity` WRITE;
/*!40000 ALTER TABLE `order_item_entity` DISABLE KEYS */;
INSERT INTO `order_item_entity` VALUES ('03356ff6-fdca-4c79-bb93-9351e4fc21f6','2023-12-03 07:32:11.439022',NULL,NULL,'Lenovo ThinkPad X1 Carbon Gen 11',26990000,1,'082fafa9-2776-4d76-828a-92543e8576c5','a9527829-6245-4906-9aa3-e996b7cf951e'),('041aa97e-f865-48e3-a84c-d4cc72c12036','2023-12-03 07:21:41.482808',NULL,NULL,'Lenovo ThinkPad X1 Carbon Gen 11',26990000,1,'fab859f1-5bba-4145-821d-c41549687f4b','a9527829-6245-4906-9aa3-e996b7cf951e'),('21e387ae-b80d-4a33-bf76-cf4d6429ddd1','2023-12-02 12:13:51.216394',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,4,'457f8cf2-f805-43b0-8d90-5f4865745ba3','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('2a75cd1d-de0d-47cb-8f8c-b6bd6e92f55b','2023-12-02 12:13:08.265067',NULL,NULL,'Dell Inspiron 16 5630 - i5 1335U, 8GB, 512GB, 2.5K - Plantium Silver - Used, Nhập khẩu',24990000,5,'acce1d72-54bb-40c1-bacb-3a3bd43fef0d','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('3213d2f8-64fd-48a2-9271-f2c892b47b71','2023-12-02 12:13:08.376698',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,3,'acce1d72-54bb-40c1-bacb-3a3bd43fef0d','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('3a13a3c7-3329-400f-9bab-401922719cf1','2023-12-03 07:27:17.503218',NULL,NULL,'Lenovo ThinkPad X1 Carbon Gen 11',26990000,1,'4b9e343d-f68a-4935-9221-81b6fc717662','a9527829-6245-4906-9aa3-e996b7cf951e'),('9fb15451-3d84-4641-882c-2cd56e7bb10e','2023-12-03 07:32:11.449306',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,1,'082fafa9-2776-4d76-828a-92543e8576c5','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('b94e2526-0e2a-446d-979f-9c8bd6571c5b','2023-12-02 12:13:51.210305',NULL,NULL,'Dell Inspiron 16 5630 - i5 1335U, 8GB, 512GB, 2.5K - Plantium Silver - Used, Nhập khẩu',24990000,2,'457f8cf2-f805-43b0-8d90-5f4865745ba3','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('baa5986b-82c7-476e-b8b1-d0ede521835a','2023-12-03 07:26:51.209228',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,1,'d2b58a49-cf5b-4e9b-ae4c-6f78c9e99028','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('c8ea1bf0-92da-4a80-89d2-0f91f26d1bcf','2023-12-03 07:27:17.515790',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,1,'4b9e343d-f68a-4935-9221-81b6fc717662','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('f6121831-6c6b-4d3b-8522-cf91db9c61dc','2023-12-03 07:21:41.553720',NULL,NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',24990000,1,'fab859f1-5bba-4145-821d-c41549687f4b','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('fa880fb5-9962-468b-a6c6-e5bde7bdc308','2023-12-03 07:26:51.199290',NULL,NULL,'Lenovo ThinkPad X1 Carbon Gen 11',26990000,1,'d2b58a49-cf5b-4e9b-ae4c-6f78c9e99028','a9527829-6245-4906-9aa3-e996b7cf951e');
/*!40000 ALTER TABLE `order_item_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attribute_entity`
--

DROP TABLE IF EXISTS `product_attribute_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_attribute_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf7u5lln5u3rggcmajqtay4ug4` (`category_id`),
  CONSTRAINT `FKf7u5lln5u3rggcmajqtay4ug4` FOREIGN KEY (`category_id`) REFERENCES `category_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attribute_entity`
--

LOCK TABLES `product_attribute_entity` WRITE;
/*!40000 ALTER TABLE `product_attribute_entity` DISABLE KEYS */;
INSERT INTO `product_attribute_entity` VALUES ('013e2736-6a66-4751-9fa9-a3c1c1c2d269','2023-11-29 11:42:39.977445',NULL,NULL,'Kết nối qua','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('03f88f98-9e98-4d2c-93f7-734c95286c0e','2023-11-29 11:47:03.103808',NULL,NULL,'Tấm nền','892b81a5-be0d-4c4f-9850-f7058f963e93'),('04053ae9-22cc-45e3-bc83-134375546c4d','2023-11-29 11:39:34.731779',NULL,NULL,'Ổ cứng','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('0baa539b-a9c6-44a4-9a70-e51ac8758310','2023-11-29 11:39:34.671702',NULL,NULL,'Bộ xử lý','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('0c20858c-b3b4-4840-a04a-bedc9b1313aa','2023-11-29 11:39:34.760583',NULL,NULL,'Webcam và Âm thanh','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('0d1b31f0-1e84-4274-81f6-11018d0e11f6','2023-11-29 11:47:03.151614',NULL,NULL,'Tiện ích','892b81a5-be0d-4c4f-9850-f7058f963e93'),('0f9bf27f-56ed-4011-9799-b4d7e647fe22','2023-11-29 11:47:03.082899',NULL,NULL,'Màn hình','892b81a5-be0d-4c4f-9850-f7058f963e93'),('1c54a1e0-e9eb-428f-bbb6-c3e87373f52f','2023-11-29 11:47:03.113428',NULL,NULL,'Số lượng màu','892b81a5-be0d-4c4f-9850-f7058f963e93'),('2f543ab4-b98c-47a8-931d-fed6ab05db3b','2023-11-29 11:47:03.142363',NULL,NULL,'Góc nhìn','892b81a5-be0d-4c4f-9850-f7058f963e93'),('3360a1b5-1466-4884-aba1-df3cbc99ebcc','2023-11-29 11:42:40.009935',NULL,NULL,'Layout','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('37ddad82-49c6-4048-9d63-6c5bd699f728','2023-11-29 11:42:39.989019',NULL,NULL,'Loại kết nối','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('43ea2d79-06ad-456f-a7cb-e12588225dcd','2023-11-29 11:39:34.713679',NULL,NULL,'Card đồ họa','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('482bf0dd-2ebd-45a3-a840-f7328e5555eb','2023-11-29 11:39:34.724909',NULL,NULL,'RAM','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('5dba42c0-203c-4a50-a70c-0e312494f125','2023-11-29 11:39:34.753476',NULL,NULL,'Pin','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('627fcf93-1bdd-447b-9abf-c69fdbdf9b98','2023-11-29 11:42:40.053223',NULL,NULL,'Pin','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('827acdf1-07c1-4997-8722-a80787b78539','2023-11-29 11:47:03.121728',NULL,NULL,'Độ tương phản','892b81a5-be0d-4c4f-9850-f7058f963e93'),('890c596c-dae0-4477-b6e4-7447947fd971','2023-11-29 11:39:34.739572',NULL,NULL,'Màn hình','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('8bbe6632-a382-42f1-bb7b-f7826fc29871','2023-11-29 11:47:03.131679',NULL,NULL,'Thời gian đáp ứng','892b81a5-be0d-4c4f-9850-f7058f963e93'),('92cbc65a-d436-4ca1-9975-79bde8bdc119','2023-11-29 11:42:40.061465',NULL,NULL,'Kích thước','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('96a797d0-5033-4966-90d3-20be464decb7','2023-11-29 11:47:03.158673',NULL,NULL,'Cổng kết nối','892b81a5-be0d-4c4f-9850-f7058f963e93'),('a86cee8a-f9e5-4700-8b54-390281199aeb','2023-11-29 11:47:03.073875',NULL,NULL,'Loại màn hình','892b81a5-be0d-4c4f-9850-f7058f963e93'),('baab724e-320a-4689-8f08-bea0fa2cf27f','2023-11-29 11:42:40.017428',NULL,NULL,'Số nút bấm','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('beed6ddf-be62-4b33-855f-8c1cbdbfeddc','2023-11-29 11:42:40.027539',NULL,NULL,'Tương thích','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('dd4738b0-2580-4bcc-8caf-a60080f04e6b','2023-11-29 11:47:03.092744',NULL,NULL,'Công nghệ màn hình','892b81a5-be0d-4c4f-9850-f7058f963e93'),('e8beeec0-d7ad-4456-a289-4a338ff848a3','2023-11-29 11:42:40.038584',NULL,NULL,'Đèn led','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('f02a6c93-4c52-47f9-a77d-90958d6c5b01','2023-11-29 11:42:40.001006',NULL,NULL,'Chất liệu khung','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('f29f7528-2620-4607-8403-e1831c137d64','2023-11-29 11:42:40.046922',NULL,NULL,'Keycap','d150ac30-ed4d-4d27-a6a9-085d7517939c'),('fb81e1e4-a5e9-4a76-9837-62bb8a2acaee','2023-11-29 11:39:34.746964',NULL,NULL,'Khối lượng & Kích thước','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec');
/*!40000 ALTER TABLE `product_attribute_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_entity`
--

DROP TABLE IF EXISTS `product_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `guarantee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `on_sale` bit(1) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `sold` int DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `brand_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8kxxmqdokh3lthvw0t148w0bc` (`category_id`),
  KEY `FKj94dnak6ecn8sf4oc862mndw3` (`brand_id`),
  CONSTRAINT `FK8kxxmqdokh3lthvw0t148w0bc` FOREIGN KEY (`category_id`) REFERENCES `category_entity` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKj94dnak6ecn8sf4oc862mndw3` FOREIGN KEY (`brand_id`) REFERENCES `brand_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_entity`
--

LOCK TABLES `product_entity` WRITE;
/*!40000 ALTER TABLE `product_entity` DISABLE KEYS */;
INSERT INTO `product_entity` VALUES ('024304be-9895-4a04-89e7-a765149c8897','Đen','2023-12-04 02:40:21.100013',NULL,'Màn hình MSI PRO MP243X 23.8 inch FHD sở hữu tần số quét 100 Hz, thời gian đáp ứng 4 ms mang đến cho bạn trải nghiệm hình ảnh tuyệt vời, cùng 119% sRGB và những thông số cao, hiện đại. Chiếc màn hình đồ họa này chắc chắn sẽ đáp ứng đầy đủ bạn từ các nhu cầu làm việc, giải trí thường ngày đến chơi game hay làm việc đồ họa.\n• Màn hình máy tính MSI có kích thước 23.8 inch sử dụng tấm nền IPS cùng độ phân giải Full HD (1920 x 1080) mang đến không gian rộng rãi cùng chất lượng hình ảnh sắc nét để bạn thoải mái trải nghiệm các nội dung khác nhau. Hơn nữa, công nghệ chống chói Anti-Glare làm hạn chế tình trạng bóng gương, tăng cường khả năng quan sát của người dùng dưới điều kiện ánh sáng cao.\n• Với tần số quét 100 Hz và thời gian đáp ứng chỉ 4 ms, màn hình cung cấp hình ảnh mượt mà và không bị giật, giảm thiểu các tình trạng nhòa mờ, duy trì tốt độ chính xác cho các chuyển động, hỗ trợ tốt khi bạn trải nghiệm các bộ phim hành động hay các tựa game có tốc độ chuyển cảnh nhanh\n• Đặc biệt, chiếc màn hình này còn có độ phủ màu đạt 119% sRGB đem đến khả năng tái tạo màu sắc chuẩn xác khi bạn làm việc đồ họa hay chỉnh sửa hình ảnh, video. MSI còn trang bị tính năng Eye Care, được thiết kế đặc biệt để bảo vệ sức khỏe của mắt bằng cách điều chỉnh tự động độ sáng và màu sắc để tối ưu hóa trải nghiệm xem mà không gây căng thẳng cho mắt khi sử dụng lâu.\n• Tính năng giảm ánh sáng xanh được tích hợp trên màn hình giúp giảm bớt ánh sáng xanh gây mỏi mắt và gây ảnh hưởng đến giấc ngủ. Kết hợp với tính năng Anti-Flicker cũng giúp giảm nhấp nháy không chỉ gây phiền hà khi xem, mà còn gây mệt mỏi và khó chịu cho mắt, từ đó đảm bảo bạn có một trải nghiệm xem mượt mà và không gây khó chịu cho mắt.\n• Màn hình máy tính được trang bị loa tích hợp để mang đến cho người dùng trải nghiệm âm thanh tốt nhất trong khi làm việc hoặc giải trí. Ngoài ra, với khả năng gắn ARM chuẩn VESA, bạn có thể dễ dàng thay đổi vị trí màn hình theo ý muốn, điều chỉnh độ cao, nghiêng, quay và xoay một cách linh hoạt.\n• MSI PRO MP243X được hoàn thiện với kiểu dáng tinh tế, hiện đại rất phù hợp để đặt trong phòng làm việc cá nhân, văn phòng hay các quầy lễ tân,... Khối lượng tổng thể 4.15 kg cũng không quá khó khăn khi di chuyển và lắp đặt.\n• Màn hình cũng có DisplayPort, HDMI, Headphone-out cho phép bạn kết nối nhanh chóng với các thiết bị như máy tính, loa và nhiều thiết bị khác.','12 tháng',NULL,'Màn hình MSI PRO MP243X 23.8 inch FHD/IPS/100Hz/4ms/HDMI  ',_binary '',2790000,8,2190000,0,_binary '','5d8b3b2a-3eeb-47af-ae94-e032d01360a9','892b81a5-be0d-4c4f-9850-f7058f963e93'),('61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','Bạc','2023-12-01 04:01:51.364483',NULL,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor recusandae eius veritatis amet, voluptatibus officia quaerat assumenda pariatur aspernatur eum quidem! Praesentium odio amet nostrum inventore! Earum ex dolorum ipsa omnis eaque temporibus dolor error suscipit qui, laudantium quod doloribus! Eius, libero! Reprehenderit, commodi. Provident illo facilis quo porro nesciunt quam vel cum iusto, quos placeat. Labore ab sequi reiciendis ipsum neque quaerat autem repudiandae, aperiam mollitia pariatur soluta ea voluptatum unde dolorem minus dolore possimus rerum magnam voluptates eum numquam placeat consectetur. Debitis possimus ex magnam rem amet aperiam. Adipisci dolore molestias eveniet reiciendis ea corporis dicta enim eligendi.','12 tháng',NULL,'Dell Inspiron 16 5630 - i5 1335U, 8GB, 512GB, 2.5K - Plantium Silver - Used, Nhập khẩu',_binary '\0',24990000,20,0,0,_binary '\0','5bbc089b-ad2b-4525-80e4-a0ec678b90b2','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('a9527829-6245-4906-9aa3-e996b7cf951e','Đen','2023-11-29 12:55:23.171236',NULL,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta repellendus molestias error aliquid blanditiis inventore sapiente ipsa, nulla repudiandae perferendis eveniet quisquam sed voluptas ea enim? Explicabo quaerat ipsum harum!','12 tháng',NULL,'Lenovo ThinkPad X1 Carbon Gen 11',_binary '\0',39990000,10,26990000,0,_binary '\0','a7e95441-bd05-47fe-843e-54876395ddf8','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec'),('f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','Đen','2023-12-01 13:00:55.243599',NULL,'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor recusandae eius veritatis amet, voluptatibus officia quaerat assumenda pariatur aspernatur eum quidem! Praesentium odio amet nostrum inventore! Earum ex dolorum ipsa omnis eaque temporibus dolor error suscipit qui, laudantium quod doloribus! Eius, libero! Reprehenderit, commodi. Provident illo facilis quo porro nesciunt quam vel cum iusto, quos placeat. Labore ab sequi reiciendis ipsum neque quaerat autem repudiandae, aperiam mollitia pariatur soluta ea voluptatum unde dolorem minus dolore possimus rerum magnam voluptates eum numquam placeat consectetur. Debitis possimus ex magnam rem amet aperiam. Adipisci dolore molestias eveniet reiciendis ea corporis dicta enim eligendi.','12 tháng',NULL,'Lenovo ThinkPad X1 Nano Gen 2 - i5 1240P, 16GB, 512GB, 2K - Black - Outlet, Nhập khẩu',_binary '\0',37990990,15,24990000,0,_binary '\0','a7e95441-bd05-47fe-843e-54876395ddf8','c4cb9cdb-59b5-484c-bdd3-92d86772b2ec');
/*!40000 ALTER TABLE `product_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image_entity`
--

DROP TABLE IF EXISTS `product_image_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_image_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3r5d73df73gohcix5pab4wdqw` (`product_id`),
  CONSTRAINT `FK3r5d73df73gohcix5pab4wdqw` FOREIGN KEY (`product_id`) REFERENCES `product_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image_entity`
--

LOCK TABLES `product_image_entity` WRITE;
/*!40000 ALTER TABLE `product_image_entity` DISABLE KEYS */;
INSERT INTO `product_image_entity` VALUES ('018ab98b-0ef9-4245-b315-1be68a0af465','2023-12-01 13:00:56.202378',NULL,NULL,'01HGJQFH827KTRK3JSTYAB5HP8.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('0347d639-2669-417b-98e5-1acfd5d203f9','2023-12-04 02:40:22.555717',NULL,NULL,'01HGSB5DKWK2TE92N37W0QCR87.jpg','024304be-9895-4a04-89e7-a765149c8897'),('095772ed-9e69-401d-96c0-7be1d4145a91','2023-12-01 04:01:53.940448',NULL,NULL,'01HGHRMHBPSJ9QCSH1CC8JP0EN.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('0a44cdd9-429a-4952-855e-5405218aa189','2023-12-04 02:40:24.261055',NULL,NULL,'01HGSB5F8WA7KQDSARTGYPY5C8.jpg','024304be-9895-4a04-89e7-a765149c8897'),('0f4e1ad1-b262-43f1-9565-fb14c2775d69','2023-12-01 04:01:52.167341',NULL,NULL,'01HGHRMFAH0RY1JSQ3Q0ZFBJRK.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('42378624-2b84-4bc5-8104-1157f82c19dc','2023-12-04 02:40:22.132391',NULL,NULL,'01HGSB5D6SX7NPDKQM4033PAAT.jpg','024304be-9895-4a04-89e7-a765149c8897'),('458bed39-2c94-4e60-bd56-2758dc09aaf9','2023-12-01 04:01:53.516469',NULL,NULL,'01HGHRMGXRD2PP9RZM5N7H6QJ8.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('472ff75c-33e5-4d10-ac9a-beac1cbcbbd7','2023-12-04 02:40:22.988809',NULL,NULL,'01HGSB5E13K35SQZB0JD8YYXVQ.jpg','024304be-9895-4a04-89e7-a765149c8897'),('583afa80-5051-476e-8a11-1dc656da7c21','2023-12-04 02:40:23.828917',NULL,NULL,'01HGSB5EVW21ZPPG45RNY3C7HD.jpg','024304be-9895-4a04-89e7-a765149c8897'),('623c20c5-3db4-47fb-ad28-be202a80efed','2023-12-01 13:00:58.778531',NULL,NULL,'01HGJQFM3HJQDDVP841MHHHZNX.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('68e40e23-0d65-4f6c-8ef4-0bda1405143d','2023-12-01 04:01:52.647679',NULL,NULL,'01HGHRMG29NK16DFN4EYKT06PK.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('7b28ebd3-9372-49bf-8fd9-6a0927db9a28','2023-11-29 12:55:23.783061',NULL,NULL,'01HGDJBYSDQCC1KJTWMCJV3WH8.png','a9527829-6245-4906-9aa3-e996b7cf951e'),('7c14833b-b5a4-45e7-9923-16dc23d8619e','2023-12-01 13:00:58.345317',NULL,NULL,'01HGJQFKQ9XA6NCB9F36SFQ1V9.png','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('93bc18e9-b083-40ea-85f8-8853a7a312dc','2023-12-01 04:01:53.072007',NULL,NULL,'01HGHRMGGGM1R31A3EGY1A7E3C.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('99ada649-0b0a-4c29-8d02-3dd56dea60bd','2023-12-01 04:01:54.356829',NULL,NULL,'01HGHRMHRWB2A89A2ACRMD9KWA.webp','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56'),('b398cec9-05da-4644-88b3-0d5f5b06af2e','2023-12-01 13:00:56.667316',NULL,NULL,'01HGJQFJ1DDG2VARSBANDDXXJE.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('b5a5f193-063b-40d7-afa2-e571f0a18b80','2023-12-01 13:00:57.952293',NULL,NULL,'01HGJQFK9Y62VEFTEVX019863K.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('b6cdcb86-c421-4113-8a7c-fa2ada844cd0','2023-11-29 12:55:24.220596',NULL,NULL,'01HGDJBZCX7BDHJBZ6CZEFHCCG.webp','a9527829-6245-4906-9aa3-e996b7cf951e'),('c3b2ea74-1484-4c82-b264-8e777060e6c1','2023-12-01 13:00:57.090176',NULL,NULL,'01HGJQFJF6D7T8J002KX4FASQM.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('d174140f-fcc8-400a-bde9-514bff9724d3','2023-12-04 02:40:23.411286',NULL,NULL,'01HGSB5EEM0EZSRNQ49TGJHENZ.jpg','024304be-9895-4a04-89e7-a765149c8897'),('db29e1c6-eba1-43b1-bc46-16cd26097c4e','2023-12-04 02:40:21.712518',NULL,NULL,'01HGSB5CKSJWM2D31FXE09JN27.jpg','024304be-9895-4a04-89e7-a765149c8897'),('efc86bf4-551c-4d3d-8e16-a5666671e3b2','2023-12-01 13:00:57.526800',NULL,NULL,'01HGJQFJWB5D2M6HW25F2G1B3R.webp','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce'),('f9a8cb1d-0ced-4ec3-ae0b-cedac89e4a89','2023-11-29 12:55:24.642260',NULL,NULL,'01HGDJBZT493DQE539AJ2DN7YW.webp','a9527829-6245-4906-9aa3-e996b7cf951e');
/*!40000 ALTER TABLE `product_image_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_spec_entity`
--

DROP TABLE IF EXISTS `product_spec_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_spec_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `product_attribute_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3r5d73df73gohcix5pab4wdqu` (`product_id`),
  KEY `FK66y2bqmh0ngw6s047w2nngtmj` (`product_attribute_id`),
  CONSTRAINT `FK3r5d73df73gohcix5pab4wdqu` FOREIGN KEY (`product_id`) REFERENCES `product_entity` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK66y2bqmh0ngw6s047w2nngtmj` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attribute_entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_spec_entity`
--

LOCK TABLES `product_spec_entity` WRITE;
/*!40000 ALTER TABLE `product_spec_entity` DISABLE KEYS */;
INSERT INTO `product_spec_entity` VALUES ('0c503bb0-ef51-493f-92e1-e64abe0f7542','2023-12-04 02:40:24.366004',NULL,NULL,'[\"Anti-Flicker\",\"Eye Care\",\"Chống chói Anti-Glare\",\"Eye Comfort\",\"119% sRGB\",\"Giảm ánh sáng xanh\"]','024304be-9895-4a04-89e7-a765149c8897','dd4738b0-2580-4bcc-8caf-a60080f04e6b'),('0f1f221f-4e87-4360-824e-572dcdd701f6','2023-12-01 13:00:58.831316',NULL,NULL,'[\"Card onboard: Intel Iris Xe Graphics\", \"Card rời: Không\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','43ea2d79-06ad-456f-a7cb-e12588225dcd'),('184eadc4-8bc5-4770-8eb7-4769aa4c5946','2023-12-01 04:01:54.366070',NULL,NULL,'[\"Loại CPU: Intel Core i5-1335U, 10C/12T\", \"Tốc độ: 1.0GHz , Lên tới 4,6GHz\", \"Bộ nhớ đệm: 12MB\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','0baa539b-a9c6-44a4-9a70-e51ac8758310'),('23683129-852f-4684-9a4b-a67142836ac3','2023-12-01 04:01:54.441660',NULL,NULL,'[\"Webcam: Có\", \"Công nghệ âm thanh: Dolby Atmos\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','0c20858c-b3b4-4840-a04a-bedc9b1313aa'),('2615e4f7-4bb5-4ff1-a1a9-719a3b8de0c0','2023-11-29 12:55:24.677687',NULL,NULL,'[\"Dung lượng SSD: 512GB (M.2 NVMe)\", \"Khả năng nâng cấp: Hỗ trợ 1 SSD\"]','a9527829-6245-4906-9aa3-e996b7cf951e','04053ae9-22cc-45e3-bc83-134375546c4d'),('2b06b4dd-8434-4a94-8d6a-896a390ff4bd','2023-11-29 12:55:24.660704',NULL,NULL,'[\"Card onboard: Intel Iris Xe Graphics\", \"Card rời: Không\"]','a9527829-6245-4906-9aa3-e996b7cf951e','43ea2d79-06ad-456f-a7cb-e12588225dcd'),('2e93935b-b2da-4031-a8cf-fba7990826e8','2023-12-04 02:40:24.356440',NULL,NULL,'[\"Phẳng\"]','024304be-9895-4a04-89e7-a765149c8897','a86cee8a-f9e5-4700-8b54-390281199aeb'),('34ae0698-695f-4b7c-bcd5-ba03298532c9','2023-12-01 13:00:58.847673',NULL,NULL,'[\"Dung lượng SSD: 512GB (M.2 NVMe)\", \"Khả năng nâng cấp: Hỗ trợ 1 SSD\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','04053ae9-22cc-45e3-bc83-134375546c4d'),('3fabca73-7fc9-497a-b2d1-bfaea564e343','2023-12-01 13:00:58.868337',NULL,NULL,'[\"Dung lượng pin: 48WHr Li-Polymer\", \"Thời lượng Pin: 6Hrs\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','5dba42c0-203c-4a50-a70c-0e312494f125'),('4975c66a-8324-4370-9dd9-067a12ff83ea','2023-11-29 12:55:24.711495',NULL,NULL,'[\"HD 720p\"]','a9527829-6245-4906-9aa3-e996b7cf951e','0c20858c-b3b4-4840-a04a-bedc9b1313aa'),('518979e4-7fbd-4f55-980a-b01597da5185','2023-12-01 13:00:58.788725',NULL,NULL,'[\"Loại CPU: Intel Core i5 1240P, 12C/16T\", \"Tốc độ: 1.0GHz , Lên tới 4.4GHz\", \"Bộ nhớ đệm: 12MB\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','0baa539b-a9c6-44a4-9a70-e51ac8758310'),('5f13ccdc-2a37-4270-9741-685d58070719','2023-11-29 12:55:24.688005',NULL,NULL,'[\"Màn hình: 14inches, 1920 x 1200px, 60Hz\", \"Tấm phủ: Nhám, Không cảm ứng\", \"Thông số khác: Tỉ lệ 16:10, IPS, 400Nits, 100% sRGB\"]','a9527829-6245-4906-9aa3-e996b7cf951e','890c596c-dae0-4477-b6e4-7447947fd971'),('68ae10ac-dd5f-40e8-b263-b16a35d17b6a','2023-11-29 12:55:24.696916',NULL,NULL,'[\"Khối lượng: 1.12kg\", \"Kích thước: 315.6mm x 222.5mm x 15.36mm\", \"Chất liệu vỏ: Hợp kim\"]','a9527829-6245-4906-9aa3-e996b7cf951e','fb81e1e4-a5e9-4a76-9837-62bb8a2acaee'),('77d4dcbd-a0bc-4e8b-8175-bc87d0e58a05','2023-12-04 02:40:24.291989',NULL,NULL,'[\"23.8 inch\",\"Full HD (1920 x 1080)\",\"100 Hz\"]','024304be-9895-4a04-89e7-a765149c8897','0f9bf27f-56ed-4011-9799-b4d7e647fe22'),('7f0395a1-3ab9-4067-9e98-f384e7d3ad4f','2023-12-01 04:01:54.428339',NULL,NULL,'[\"Khối lượng: 1.85kg\", \"Kích thước: 356.78mm x 251.7mm x 18.2mm\", \"Chất liệu vỏ: Kim loại\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','fb81e1e4-a5e9-4a76-9837-62bb8a2acaee'),('8cbba757-eae8-4cb6-a6fc-9c1ee4d7b447','2023-12-04 02:40:24.281362',NULL,NULL,'[\"Điều chỉnh được độ nghiêng của màn hình\"]','024304be-9895-4a04-89e7-a765149c8897','0d1b31f0-1e84-4274-81f6-11018d0e11f6'),('960bd376-66b8-455d-96d4-aa40bd557365','2023-12-04 02:40:24.310708',NULL,NULL,'[\"178°(Dọc) / 178°(Ngang)\"]','024304be-9895-4a04-89e7-a765149c8897','2f543ab4-b98c-47a8-931d-fed6ab05db3b'),('aed4d3c6-002d-4b96-94ae-94db0e614b06','2023-12-01 13:00:58.838036',NULL,NULL,'[\"Dung lượng: 16GB LPDDR5 5200MHz\", \"Hỗ trợ tối đa: 16GB (Không thể nâng cấp)\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','482bf0dd-2ebd-45a3-a840-f7328e5555eb'),('b48ae456-7e61-4489-a95b-9d2e75e29c4b','2023-12-01 04:01:54.421152',NULL,NULL,'[\"Màn hình: 16inches, 2560 x 1600px, 60Hz\", \"Tấm phủ: Nhám, Không cảm ứng\", \"Thông số khác: Tỉ lệ 16:10, WVA, 250Nits, 100% sRGB\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','890c596c-dae0-4477-b6e4-7447947fd971'),('b4c78de7-2e52-4356-b902-97ca525d98f7','2023-12-01 13:00:58.854730',NULL,NULL,'[\"Màn hình: 13inches, 2160 x 1350px, 60Hz\", \"Tấm phủ: Nhám, Không cảm ứng\", \"Thông số khác: Tỉ lệ 16:10, IPS, 450Nits, 100% sRGB\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','890c596c-dae0-4477-b6e4-7447947fd971'),('b7d374a8-e8c6-4142-8dc1-d34f2b154c05','2023-12-01 04:01:54.395941',NULL,NULL,'[\"Card onboard: Intel Iris Xe Graphics\", \"Card rời: Không\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','43ea2d79-06ad-456f-a7cb-e12588225dcd'),('b8351359-79ad-4306-8270-e70af1334cad','2023-12-04 02:40:24.271084',NULL,NULL,'[\"IPS\"]','024304be-9895-4a04-89e7-a765149c8897','03f88f98-9e98-4d2c-93f7-734c95286c0e'),('b88180ea-d0d5-4ac3-8326-0bd3effa971b','2023-11-29 12:55:24.669372',NULL,NULL,'[\"Dung lượng: 16GB LPDDR5 6400MHz\", \"Hỗ trợ tối đa: 16GB (Không thể nâng cấp)\"]','a9527829-6245-4906-9aa3-e996b7cf951e','482bf0dd-2ebd-45a3-a840-f7328e5555eb'),('ba6d7aaf-28b6-4c00-87ba-dcf6161de108','2023-11-29 12:55:24.704212',NULL,NULL,'[\"Dung lượng pin: 57WHr Li-Polymer\", \"Thời lượng Pin: 5Hrs\"]','a9527829-6245-4906-9aa3-e996b7cf951e','5dba42c0-203c-4a50-a70c-0e312494f125'),('c0a621a0-d443-4348-a8d8-8e717d54dff4','2023-12-01 04:01:54.412490',NULL,NULL,'[\"Dung lượng SSD: 512GB (M.2 NVMe)\", \"Khả năng nâng cấp: Hỗ trợ 1 SSD\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','04053ae9-22cc-45e3-bc83-134375546c4d'),('c2d35a3d-615a-45e0-92b3-2e71edfd33e8','2023-12-04 02:40:24.299991',NULL,NULL,'[\"16.7 triệu màu\"]','024304be-9895-4a04-89e7-a765149c8897','1c54a1e0-e9eb-428f-bbb6-c3e87373f52f'),('c7a2265e-0295-421f-af49-abcb936a19c0','2023-12-04 02:40:24.318995',NULL,NULL,'[\"1000:1\"]','024304be-9895-4a04-89e7-a765149c8897','827acdf1-07c1-4997-8722-a80787b78539'),('d17db9ab-f164-42a3-bee3-43e99fdf852c','2023-12-04 02:40:24.329419',NULL,NULL,'[\"4ms (GTG)\"]','024304be-9895-4a04-89e7-a765149c8897','8bbe6632-a382-42f1-bb7b-f7826fc29871'),('d9663c20-0406-49e2-9de9-c4b12eee6526','2023-12-01 13:00:58.860763',NULL,NULL,'[\"Khối lượng: 0.97kg\", \"Kích thước: 356.78mm x 251.7mm x 18.2mm\", \"Chất liệu vỏ: Kim loại\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','fb81e1e4-a5e9-4a76-9837-62bb8a2acaee'),('e2a16846-71cb-4a17-9cb3-8a31f2693d77','2023-12-01 04:01:54.435339',NULL,NULL,'[\"Dung lượng pin: 54WHr Li-Polymer\", \"Thời lượng Pin: 5Hrs\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','5dba42c0-203c-4a50-a70c-0e312494f125'),('e80b03b7-055a-4a6c-a621-303c985a7207','2023-11-29 12:55:24.651970',NULL,NULL,'[\"Loại CPU: Intel Core i5 1335U, 10C/12T\", \"Tốc độ: 1.0GHz , Lên tới 4.6GHz\", \"Bộ nhớ đệm: 12MB\"]','a9527829-6245-4906-9aa3-e996b7cf951e','0baa539b-a9c6-44a4-9a70-e51ac8758310'),('fa60d0ec-70cb-4270-aa8f-c1d7f5078dd9','2023-12-01 04:01:54.404819',NULL,NULL,'[\"Dung lượng: 8GB LPDDR5 5200MHz\", \"Hỗ trợ tối đa: 16GB (Không thể nâng cấp)\"]','61ef4d2c-050a-4cec-81a8-3ea8c0f8ad56','482bf0dd-2ebd-45a3-a840-f7328e5555eb'),('ff4acbd5-a388-49a4-b46a-282310a1bce9','2023-12-01 13:00:58.877220',NULL,NULL,'[\"Webcam: Có\", \"Công nghệ âm thanh: Dolby Atmos® Speaker System\"]','f77e48b1-c9e1-4caf-99cf-ffad4a2c0cce','0c20858c-b3b4-4840-a04a-bedc9b1313aa'),('ff5d5d45-9d94-43ca-ad41-8949d286fa4a','2023-12-04 02:40:24.335841',NULL,NULL,'[\"1 x Headphone-out\",\"1 x DisplayPort 1.2\",\"1 x HDMI 1.4\"]','024304be-9895-4a04-89e7-a765149c8897','96a797d0-5033-4966-90d3-20be464decb7');
/*!40000 ALTER TABLE `product_spec_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_entity`
--

DROP TABLE IF EXISTS `user_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_entity` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_updated_at` datetime(6) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_entity`
--

LOCK TABLES `user_entity` WRITE;
/*!40000 ALTER TABLE `user_entity` DISABLE KEYS */;
INSERT INTO `user_entity` VALUES ('53420496-bccf-4df5-adea-59e44cf8ec8e','2023-12-01 03:14:27.995126',NULL,'20110160@student.hcmute.edu.vn','Trần Văn Quảng',NULL,'$2a$11$D/gXjk4Sxa7qrFRx7YOgH.UVDUxmJ8tHWmMIbQgnKtAFhwx.ahlzi','USER',_binary '\1'),('6eba19db-7136-4103-8916-303768613ef5','2023-11-18 15:29:42.741609',NULL,'admin@gmail.com','Admin',NULL,'$2a$11$Cq9rmB6GCytb2WhSiqaPCO6QZd/V6XfS7l6upRemDk/wzJzCDP7hq','ADMIN',_binary '\1'),('c8d8e43c-5a3b-4b5d-90fa-36a421611826','2023-12-02 10:15:24.249834',NULL,'duy@gmail.com','Ngoc Duy',NULL,'$2a$11$KPQURfMid9GwGapkDNH9neNODFezXc8q0BJoSCwe7t1UHpSmmlacS','USER',_binary '\1');
/*!40000 ALTER TABLE `user_entity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-04  3:16:55